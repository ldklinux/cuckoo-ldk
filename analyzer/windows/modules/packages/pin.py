# Copyright (C) 2010-2014 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.common.abstracts import Package
from lib.api.process import Process
from lib.common.exceptions import CuckooPackageError
from lib.common.results import NetlogFile
import time
import os
import glob


class Pin(Package):
    """PIN analysis package."""

    def start(self, path):
        pin = "C:\\Program Files\\package\\pin.exe"
        #pindll = "C:\\Program Files\\package\\aligotTracer.dll"
        #pindllopts = "startA 401020 endA 40109F outFile C:\\trace.ott"

        # Pintool to run (must be loaded inside VM
        # in folder C:\\Program Files\\package)
        pindll = self.options.get("pindll", None)
        if not pindll:
            raise CuckooPackageError("Must specify pintoo with pindll package option"
                                     "(e.g. pindll=mypintool.dll")
        else:
            pindll = "C:\\Program Files\\package\\" + pindll
            
        # Arguments to pintool
        pinopts = self.options.get("pinopts", "")
        if "outFile" in pinopts:
            raise CuckooPackageError("Redefinition of output trace path, "
                                     "analysis aborted")
        
        # Argument to pass to program under analysis
        args = self.options.get("arguments", None)

        p = Process()
        if not p.execute(path=pin, args="-follow_execv 1 -t \"%s\" %s -- \"%s\" %s" % (pindll, pinopts, path, args), suspended=True):
            raise CuckooPackageError("Unable to execute initial process, "
                                     "analysis aborted")

        # Give time to network to come up
        time.sleep(15)
        p.resume()
        p.close()
        return p.pid

    def check(self):
        return True

    def package_files(self):
        #return [("C:\\trace.ott", "insn_trace.txt")]
        return None
    
    def finish(self):
        # The following wait is there to allow Windows to flush
        # the trace file to the VM hard disk
        time.sleep(10)
        for tn in glob.glob("C:\\pintrace*"):
            if os.path.getsize(tn) > 12:
                with open(tn, 'r') as inTr:
                    CHUNK_SIZE = 1048576
                    done = False
                    chunk_counter = 0
                    while not done:
                        chunk = inTr.read(CHUNK_SIZE)
                        if len(chunk) == 0:
                            done = True
                        else:
                            nf = NetlogFile("trace/%s_%s" % (tn[3:], str(chunk_counter).rjust(5, "0")))
                            nf.sock.sendall(chunk)
                            nf.close()
                            chunk_counter += 1                
        return True
