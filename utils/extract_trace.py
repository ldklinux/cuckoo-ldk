#!/usr/bin/env python2.7

import sys
import os
import subprocess
import glob
import time


if len(sys.argv) != 3:
    sys.stderr.write("Usage: extract_trace.py <path to VM folder> <output file name>" + os.linesep)
    sys.exit(-1)


# Part 1 - duplicate main VM disk
sys.stderr.write("# Part 1 - duplicate main VM disk..." + os.linesep)
files = glob.glob(sys.argv[1] + '/*.vdi')
assert len(files) == 1
vmdisk = files[0]
vmdest = "/tmp/%s-full.vdi" % os.path.basename(vmdisk)[:-4]
result = subprocess.call(["rsync", "-ah", "--progress", vmdisk, vmdest])
assert result == 0
sys.stderr.write("# Part 1 - duplicate main VM disk... done!" + os.linesep*2)

# Part 2 - assign a new UUID to the disk
sys.stderr.write("# Part 2 - assign a new UUID to the disk..." + os.linesep)
result = subprocess.call(["VBoxManage", "internalcommands", "sethduuid", vmdest])
assert result == 0
sys.stderr.write("# Part 2 - assign a new UUID to the disk... done!" + os.linesep*2)

# Part 3 - merge base disk image with delta image
sys.stderr.write("# Part 3 - merge base disk image with delta image..." + os.linesep)
files = glob.glob("%s/Snapshots/*.vdi" % sys.argv[1])
assert len(files) == 1
deltadisk = files[0]
result = subprocess.call(["VBoxManage", "clonehd", deltadisk, vmdest, "--existing"])
assert result == 0
sys.stderr.write("# Part 3 - merge base disk image with delta image... done!" + os.linesep*2)

# Part 4 - mount disk image using Paragon VDI mounter
sys.stderr.write("# Part 4 - mount disk image using Paragon VDI mounter..." + os.linesep)
result = subprocess.call(["vdmutil",  "attach", vmdest])
assert result == 0
sys.stderr.write("# Part 4 - mount disk image using Paragon VDI mounter... done!" + os.linesep*2)

time.sleep(5)

# Part 5 - copy trace out of virtual disk
# Note - this should really be made smarter (check where the disk is
# mounted instead of assuming "/Volumes/Untitled", parametrize the
# trace name) but it is good for now - future work! :-)
sys.stderr.write("# Part 5 - copy trace out of virtual disk..." + os.linesep)
filesrc = "/Volumes/Untitled/trace.ott"
filedest = sys.argv[2]
result = subprocess.call(["rsync", "-ah", "--progress", filesrc, filedest])
assert result == 0
sys.stderr.write("# Part 5 - copy trace out of virtual disk... done!" + os.linesep*2)

# Part 6 - umount virtual disk
# Again, this should be parameterized etc.
# Also, the disk should be ejected ("diskutil eject"), which
# also cleans up the OS notion of physical presence of the disk
sys.stderr.write("# Part 6 - umount virtual disk..." + os.linesep)
result = subprocess.call(["umount", "/Volumes/Untitled"])
assert result == 0
sys.stderr.write("# Part 6 - umount virtual disk... done!" + os.linesep*2)

# Part 7 - remove temporary disk image
sys.stderr.write("# Part 7 - remove temporary disk image..." + os.linesep)
os.remove(vmdest)
sys.stderr.write("# Part 7 - remove temporary disk image... done!" + os.linesep*2)

sys.stderr.write("# All done - Remember to unmount the disk image using disk utility, otherwise space will NOT be freed!!!" + os.linesep*2)
