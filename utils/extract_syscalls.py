#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 16:11:43 2015

@author: lorenzo
"""

import sys
import os
import datetime
import struct
import string
import time
import ntpath

###############################################################################
# Generic BSON based protocol - by rep
# Allows all kinds of languages / sources to generate input for Cuckoo,
# thus we can reuse report generation / signatures for other API trace sources
###############################################################################

LOGTBL = [
    ("__process__", "__init__", ("",)),
    ("__thread__", "__init__", ("",)),
    ("NtDeleteFile", "filesystem", ("O", "FileName")),
    ("CreateDirectoryW", "filesystem", ("u", "DirectoryName")),
    ("CreateDirectoryExW", "filesystem", ("u", "DirectoryName")),
    ("RemoveDirectoryA", "filesystem", ("s", "DirectoryName")),
    ("RemoveDirectoryW", "filesystem", ("u", "DirectoryName")),
    ("FindFirstFileExA", "filesystem", ("s", "FileName")),
    ("FindFirstFileExW", "filesystem", ("u", "FileName")),
    ("DeleteFileA", "filesystem", ("s", "FileName")),
    ("DeleteFileW", "filesystem", ("u", "FileName")),
    ("UnhookWindowsHookEx", "hooking", ("p", "HookHandle")),
    ("LdrGetDllHandle", "system", ("oP", "FileName", "ModuleHandle")),
    ("ExitWindowsEx", "system", ("ll", "Flags", "Reason")),
    ("IsDebuggerPresent", "system", ("",)),
    ("LookupPrivilegeValueW", "system", ("uu", "SystemName", "PrivilegeName")),
    ("NtClose", "system", ("p", "Handle")),
    ("URLDownloadToFileW", "network", ("uu", "URL", "FileName")),
    ("InternetReadFile", "network", ("pB", "InternetHandle", "Buffer")),
    ("InternetWriteFile", "network", ("pB", "InternetHandle", "Buffer")),
    ("InternetCloseHandle", "network", ("p", "InternetHandle")),
    ("DnsQuery_A", "network", ("sil", "Name", "Type", "Options")),
    ("DnsQuery_UTF8", "network", ("sil", "Name", "Type", "Options")),
    ("DnsQuery_W", "network", ("uil", "Name", "Type", "Options")),
    ("getaddrinfo", "network", ("ss", "NodeName", "ServiceName")),
    ("GetAddrInfoW", "network", ("uu", "NodeName", "ServiceName")),
    ("NtTerminateProcess", "process", ("pl", "ProcessHandle", "ExitCode")),
    ("ExitProcess", "process", ("l", "ExitCode")),
    ("system", "process", ("s", "Command")),
    ("RegOpenKeyExA", "registry", ("psP", "Registry", "SubKey", "Handle")),
    ("RegOpenKeyExW", "registry", ("puP", "Registry", "SubKey", "Handle")),
    ("RegDeleteKeyA", "registry", ("ps", "Handle", "SubKey")),
    ("RegDeleteKeyW", "registry", ("pu", "Handle", "SubKey")),
    ("RegEnumKeyW", "registry", ("plu", "Handle", "Index", "Name")),
    ("RegDeleteValueA", "registry", ("ps", "Handle", "ValueName")),
    ("RegDeleteValueW", "registry", ("pu", "Handle", "ValueName")),
    ("RegCloseKey", "registry", ("p", "Handle")),
    ("NtRenameKey", "registry", ("po", "KeyHandle", "NewName")),
    ("NtEnumerateKey", "registry", ("pl", "KeyHandle", "Index")),
    ("NtDeleteKey", "registry", ("p", "KeyHandle")),
    ("NtDeleteValueKey", "registry", ("po", "KeyHandle", "ValueName")),
    ("NtLoadKey", "registry", ("OO", "TargetKey", "SourceFile")),
    ("NtSaveKey", "registry", ("pp", "KeyHandle", "FileHandle")),
    ("ControlService", "services", ("pl", "ServiceHandle", "ControlCode")),
    ("DeleteService", "services", ("p", "ServiceHandle")),
    ("NtDelayExecution", "system", ("ls", "Milliseconds", "Status")),
    ("NtDelayExecution", "system", ("l", "Milliseconds")),
    ("WSAStartup", "socket", ("p", "VersionRequested")),
    ("gethostbyname", "socket", ("s", "Name")),
    ("socket", "socket", ("lll", "af", "type", "protocol")),
    ("connect", "socket", ("p", "socket")),
    ("send", "socket", ("pb", "socket", "buffer")),
    ("sendto", "socket", ("pb", "socket", "buffer")),
    ("recv", "socket", ("pb", "socket", "buffer")),
    ("recvfrom", "socket", ("pb", "socket", "buffer")),
    ("accept", "socket", ("pp", "socket", "ClientSocket")),
    ("bind", "socket", ("psl", "socket", "ip", "port")),
    ("bind", "socket", ("p", "socket")),
    ("setsockopt", "socket", ("pllb", "socket", "level", "optname", "optval")),
    ("listen", "socket", ("p", "socket")),
    ("select", "socket", ("p", "socket")),
    ("ioctlsocket", "socket", ("pl", "socket", "command")),
    ("closesocket", "socket", ("p", "socket")),
    ("shutdown", "socket", ("pl", "socket", "how")),
    ("WSARecv", "socket", ("p", "socket")),
    ("WSARecvFrom", "socket", ("p", "socket")),
    ("WSASend", "socket", ("p", "Socket")),
    ("WSASendTo", "socket", ("p", "Socket")),
    ("WSASocketA", "socket", ("lll", "af", "type", "protocol")),
    ("WSASocketW", "socket", ("lll", "af", "type", "protocol")),
    ("ConnectEx", "socket", ("pB", "socket", "SendBuffer")),
    ("NtOpenMutant", "synchronization", ("PO", "Handle", "MutexName")),
    ("NtGetContextThread", "threading", ("p", "ThreadHandle")),
    ("NtSetContextThread", "threading", ("p", "ThreadHandle")),
    ("NtResumeThread", "threading", ("pL", "ThreadHandle", "SuspendCount")),
    ("NtTerminateThread", "threading", ("pl", "ThreadHandle", "ExitStatus")),
    ("ExitThread", "threading", ("l", "ExitCode")),
    ("FindWindowA", "windows", ("ss", "ClassName", "WindowName")),
    ("FindWindowW", "windows", ("uu", "ClassName", "WindowName")),
    ("FindWindowExA", "windows", ("ls", "ClassName", "WindowName")),
    ("FindWindowExA", "windows", ("ss", "ClassName", "WindowName")),
    ("FindWindowExW", "windows", ("lu", "ClassName", "WindowName")),
    ("FindWindowExW", "windows", ("uu", "ClassName", "WindowName")),
    ("NtCreateFile", "filesystem", ("PpOll", "FileHandle", "DesiredAccess", "FileName", "CreateDisposition", "ShareAccess")),
    ("NtOpenFile", "filesystem", ("PpOl", "FileHandle", "DesiredAccess", "FileName", "ShareAccess")),
    ("NtReadFile", "filesystem", ("pb", "FileHandle", "Buffer")),
    ("NtWriteFile", "filesystem", ("pb", "FileHandle", "Buffer")),
    ("NtDeviceIoControlFile", "filesystem", ("pbb", "FileHandle", "InputBuffer", "OutputBuffer")),
    ("NtQueryDirectoryFile", "filesystem", ("pbo", "FileHandle", "FileInformation", "FileName")),
    ("NtQueryInformationFile", "filesystem", ("pb", "FileHandle", "FileInformation")),
    ("NtSetInformationFile", "filesystem", ("pb", "FileHandle", "FileInformation")),
    ("NtOpenDirectoryObject", "filesystem", ("PlO", "DirectoryHandle", "DesiredAccess", "ObjectAttributes")),
    ("NtCreateDirectoryObject", "filesystem", ("PlO", "DirectoryHandle", "DesiredAccess", "ObjectAttributes")),
    ("MoveFileWithProgressW", "filesystem", ("uu", "ExistingFileName", "NewFileName")),
    ("CopyFileA", "filesystem", ("ss", "ExistingFileName", "NewFileName")),
    ("CopyFileW", "filesystem", ("uu", "ExistingFileName", "NewFileName")),
    ("CopyFileExW", "filesystem", ("uul", "ExistingFileName", "NewFileName", "CopyFlags")),
    ("SetWindowsHookExA", "system", ("lppl", "HookIdentifier", "ProcedureAddress", "ModuleAddress", "ThreadId")),
    ("SetWindowsHookExW", "system", ("lppl", "HookIdentifier", "ProcedureAddress", "ModuleAddress", "ThreadId")),
    ("LdrLoadDll", "system", ("loP", "Flags", "FileName", "BaseAddress")),
    ("LdrGetProcedureAddress", "system", ("pSlP", "ModuleHandle", "FunctionName", "Ordinal", "FunctionAddress")),
    ("DeviceIoControl", "device", ("plbb", "DeviceHandle", "IoControlCode", "InBuffer", "OutBuffer")),
    ("WriteConsoleA", "system", ("pS", "ConsoleHandle", "Buffer")),
    ("WriteConsoleW", "system", ("pU", "ConsoleHandle", "Buffer")),
    ("InternetOpenA", "network", ("spssp", "Agent", "AccessType", "ProxyName", "ProxyBypass", "Flags")),
    ("InternetOpenW", "network", ("upuup", "Agent", "AccessType", "ProxyName", "ProxyBypass", "Flags")),
    ("InternetConnectA", "network", ("pslsslp", "InternetHandle", "ServerName", "ServerPort", "Username", "Password", "Service", "Flags")),
    ("InternetConnectW", "network", ("puluulp", "InternetHandle", "ServerName", "ServerPort", "Username", "Password", "Service", "Flags")),
    ("InternetOpenUrlA", "network", ("psSp", "ConnectionHandle", "URL", "Headers", "Flags")),
    ("InternetOpenUrlW", "network", ("puUp", "ConnectionHandle", "URL", "Headers", "Flags")),
    ("HttpOpenRequestA", "network", ("psl", "InternetHandle", "Path", "Flags")),
    ("HttpOpenRequestW", "network", ("pul", "InternetHandle", "Path", "Flags")),
    ("HttpSendRequestA", "network", ("pSb", "RequestHandle", "Headers", "PostData")),
    ("HttpSendRequestW", "network", ("pUb", "RequestHandle", "Headers", "PostData")),
    ("NtCreateProcess", "process", ("PpO", "ProcessHandle", "DesiredAccess", "FileName")),
    ("NtCreateProcessEx", "process", ("PpO", "ProcessHandle", "DesiredAccess", "FileName")),
    ("NtCreateUserProcess", "process", ("PPppOOoo", "ProcessHandle", "ThreadHandle", "ProcessDesiredAccess", "ThreadDesiredAccess", "ProcessFileName", "ThreadName", "ImagePathName", "CommandLine")),
    ("NtOpenProcess", "process", ("ppp", "ProcessHandle", "DesiredAccess", "ProcessIdentifier")),
    ("NtOpenProcess", "process", ("PpP", "ProcessHandle", "DesiredAccess", "ProcessIdentifier")),
    ("NtCreateSection", "process", ("PpOp", "SectionHandle", "DesiredAccess", "ObjectAttributes", "FileHandle")),
    ("NtOpenSection", "process", ("PpO", "SectionHandle", "DesiredAccess", "ObjectAttributes")),
    ("CreateProcessInternalW", "process", ("uupllpp", "ApplicationName", "CommandLine", "CreationFlags", "ProcessId", "ThreadId", "ProcessHandle", "ThreadHandle")),
    ("ShellExecuteExW", "process", ("2ul", "FilePath", "Parameters", "Show")),
    ("NtAllocateVirtualMemory", "process", ("pPPp", "ProcessHandle", "BaseAddress", "RegionSize", "Protection")),
    ("NtReadVirtualMemory", "process", ("2pB", "ProcessHandle", "BaseAddress", "Buffer")),
    ("ReadProcessMemory", "process", ("ppB", "ProcessHandle", "BaseAddress", "Buffer")),
    ("NtWriteVirtualMemory", "process", ("2pB", "ProcessHandle", "BaseAddress", "Buffer")),
    ("WriteProcessMemory", "process", ("ppB", "ProcessHandle", "BaseAddress", "Buffer")),
    ("NtProtectVirtualMemory", "process", ("pPPpP", "ProcessHandle", "BaseAddress", "NumberOfBytesProtected", "NewAccessProtection", "OldAccessProtection")),
    ("VirtualProtectEx", "process", ("pppp", "ProcessHandle", "Address", "Size", "Protection")),
    ("NtFreeVirtualMemory", "process", ("pPPp", "ProcessHandle", "BaseAddress", "RegionSize", "FreeType")),
    ("VirtualFreeEx", "process", ("pppl", "ProcessHandle", "Address", "Size", "FreeType")),
    ("RegCreateKeyExA", "registry", ("psslP", "Registry", "SubKey", "Class", "Access", "Handle")),
    ("RegCreateKeyExW", "registry", ("puulP", "Registry", "SubKey", "Class", "Access", "Handle")),
    ("RegEnumKeyExA", "registry", ("plss", "Handle", "Index", "Name", "Class")),
    ("RegEnumKeyExW", "registry", ("pluu", "Handle", "Index", "Name", "Class")),
    ("RegEnumValueA", "registry", ("plsr", "Handle", "Index", "ValueName", "Data")),
    ("RegEnumValueA", "registry", ("plsLL", "Handle", "Index", "ValueName", "Type", "DataLength")),
    ("RegEnumValueW", "registry", ("pluR", "Handle", "Index", "ValueName", "Data")),
    ("RegEnumValueW", "registry", ("pluLL", "Handle", "Index", "ValueName", "Type", "DataLength")),
    ("RegSetValueExA", "registry", ("pslr", "Handle", "ValueName", "Type", "Buffer")),
    ("RegSetValueExA", "registry", ("psl", "Handle", "ValueName", "Type")),
    ("RegSetValueExW", "registry", ("pulR", "Handle", "ValueName", "Type", "Buffer")),
    ("RegSetValueExW", "registry", ("pul", "Handle", "ValueName", "Type")),
    ("RegQueryValueExA", "registry", ("psr", "Handle", "ValueName", "Data")),
    ("RegQueryValueExA", "registry", ("psLL", "Handle", "ValueName", "Type", "DataLength")),
    ("RegQueryValueExW", "registry", ("puR", "Handle", "ValueName", "Data")),
    ("RegQueryValueExW", "registry", ("puLL", "Handle", "ValueName", "Type", "DataLength")),
    ("RegQueryInfoKeyA", "registry", ("pS6L", "KeyHandle", "Class", "SubKeyCount", "MaxSubKeyLength", "MaxClassLength", "ValueCount", "MaxValueNameLength", "MaxValueLength")),
    ("RegQueryInfoKeyW", "registry", ("pU6L", "KeyHandle", "Class", "SubKeyCount", "MaxSubKeyLength", "MaxClassLength", "ValueCount", "MaxValueNameLength", "MaxValueLength")),
    ("NtCreateKey", "registry", ("PlOo", "KeyHandle", "DesiredAccess", "ObjectAttributes", "Class")),
    ("NtOpenKey", "registry", ("PlO", "KeyHandle", "DesiredAccess", "ObjectAttributes")),
    ("NtOpenKeyEx", "registry", ("PlO", "KeyHandle", "DesiredAccess", "ObjectAttributes")),
    ("NtReplaceKey", "registry", ("pOO", "KeyHandle", "NewHiveFileName", "BackupHiveFileName")),
    ("NtEnumerateValueKey", "registry", ("pll", "KeyHandle", "Index", "KeyValueInformationClass")),
    ("NtSetValueKey", "registry", ("polR", "KeyHandle", "ValueName", "Type", "Buffer")),
    ("NtSetValueKey", "registry", ("pol", "KeyHandle", "ValueName", "Type")),
    ("NtQueryValueKey", "registry", ("polR", "KeyHandle", "ValueName", "Type", "Information")),
    ("NtQueryValueKey", "registry", ("po", "KeyHandle", "ValueName")),
    ("NtQueryMultipleValueKey", "registry", ("poS", "KeyHandle", "ValueName", "ValueBuffer")),
    ("NtLoadKey2", "registry", ("OOl", "TargetKey", "SourceFile", "Flags")),
    ("NtLoadKeyEx", "registry", ("pOOl", "TrustClassKey", "TargetKey", "SourceFile", "Flags")),
    ("NtQueryKey", "registry", ("pSl", "KeyHandle", "KeyInformation", "KeyInformationClass")),
    ("NtSaveKeyEx", "registry", ("ppl", "KeyHandle", "FileHandle", "Format")),
    ("OpenSCManagerA", "services", ("ssl", "MachineName", "DatabaseName", "DesiredAccess")),
    ("OpenSCManagerW", "services", ("uul", "MachineName", "DatabaseName", "DesiredAccess")),
    ("CreateServiceA", "services", ("pss4l3s", "ServiceControlHandle", "ServiceName", "DisplayName", "DesiredAccess", "ServiceType", "StartType", "ErrorControl", "BinaryPathName", "ServiceStartName", "Password")),
    ("CreateServiceW", "services", ("puu4l3u", "ServiceControlHandle", "ServiceName", "DisplayName", "DesiredAccess", "ServiceType", "StartType", "ErrorControl", "BinaryPathName", "ServiceStartName", "Password")),
    ("OpenServiceA", "services", ("psl", "ServiceControlManager", "ServiceName", "DesiredAccess")),
    ("OpenServiceW", "services", ("pul", "ServiceControlManager", "ServiceName", "DesiredAccess")),
    ("StartServiceA", "services", ("pa", "ServiceHandle", "Arguments")),
    ("StartServiceW", "services", ("pA", "ServiceHandle", "Arguments")),
    ("TransmitFile", "socket", ("ppll", "socket", "FileHandle", "NumberOfBytesToWrite", "NumberOfBytesPerSend")),
    ("NtCreateMutant", "synchronization", ("POl", "Handle", "MutexName", "InitialOwner")),
    ("NtCreateNamedPipeFile", "synchronization", ("PpOl", "NamedPipeHandle", "DesiredAccess", "PipeName", "ShareAccess")),
    ("NtCreateThread", "threading", ("PpO", "ThreadHandle", "ProcessHandle", "ObjectAttributes")),
    ("NtOpenThread", "threading", ("PlO", "ThreadHandle", "DesiredAccess", "ObjectAttributes")),
    ("NtSuspendThread", "threading", ("pL", "ThreadHandle", "SuspendCount")),
    ("CreateThread", "threading", ("pplL", "StartRoutine", "Parameter", "CreationFlags", "ThreadId")),
    ("CreateRemoteThread", "threading", ("3plL", "ProcessHandle", "StartRoutine", "Parameter", "CreationFlags", "ThreadId")),
    ("RtlCreateUserThread", "threading", ("plppPl", "ProcessHandle", "CreateSuspended", "StartAddress", "StartParameter", "ThreadHandle", "ThreadIdentifier")),
    ("ZwMapViewOfSection", "process", ("ppPp", "SectionHandle", "ProcessHandle", "BaseAddress", "SectionOffset")),
    ("GetSystemMetrics", "misc", ("l", "SystemMetricIndex")),
    ("GetCursorPos", "misc", ("ll", "x", "y")),
]

try:
    import bson
    HAVE_BSON = True
except ImportError:
    HAVE_BSON = False

if HAVE_BSON:
    # The BSON module provided by pymongo works through its "BSON" class.
    if hasattr(bson, "BSON"):
        bson_decode = lambda d: bson.BSON(d).decode()
    # The BSON module provided by "pip install bson" works through the
    # "loads" function (just like pickle etc.)
    elif hasattr(bson, "loads"):
        bson_decode = lambda d: bson.loads(d)
    else:
        HAVE_BSON = False

# 1 Mb max message length
MAX_MESSAGE_LENGTH = 20 * 1024 * 1024

PRINTABLE_CHARACTERS = string.letters + string.digits + string.punctuation + " \t\r\n"

TYPECONVERTERS = {
    "p": lambda v: "0x%08x" % default_converter(v),
}

def default_converter(v):
    # fix signed ints (bson is kind of limited there)
    if type(v) in (int, long) and v < 0:
        return v + 0x100000000
    return v

def check_names_for_typeinfo(arginfo):
    argnames = [i[0] if type(i) in (list, tuple) else i for i in arginfo]

    converters = []
    for i in arginfo:
        if type(i) in (list, tuple):
            r = TYPECONVERTERS.get(i[1], None)
            if not r:
                sys.stderr.write("Analyzer sent unknown format specifier %s" % format(i[1]) + os.linesep*2)
                r = default_converter
            converters.append(r)
        else:
            converters.append(default_converter)

    return argnames, converters

def is_printable(s):
    for c in s:
        if not c in PRINTABLE_CHARACTERS:
            return False
    return True

def convert_char(c):
    if c in PRINTABLE_CHARACTERS:
        return c
    else:
        return "\\x%02x" % ord(c)

def convert_to_printable(s):
    if is_printable(s):
        return s
    return "".join(convert_char(c) for c in s)

def logtime(dt):
    t = time.strftime("%Y-%m-%d %H:%M:%S", dt.timetuple())
    s = "%s,%03d" % (t, dt.microsecond/1000)
    return s

def cleanup_value(v):
    v = str(v)
    if v.startswith("\\??\\"):
        v = v[4:]
    return v

def get_filename_from_path(path):
    dirpath, filename = ntpath.split(path)
    return filename if filename else ntpath.basename(dirpath)
    
class logProcessor(object):
    def __init__(self, file_path):
        self.fd = open(file_path, 'r')
        self.process_id = None
        self.parent_id = None
        self.process_name = None
        self.first_seen = None
        self.calls = []

    def process(self):
        bp = BsonParser(self)                
        bp.read_all()
        
    def close(self):
        self.fd.close()
        
    def read(self, length):
        if not length:
            return ''
        buf = self.fd.read(length)
        if not buf or len(buf) != length:
            return ''
        return buf

    def log_process(self, context, timestring, pid, ppid, modulepath, procname):
        self.process_id, self.parent_id, self.process_name = pid, ppid, procname
        self.first_seen = timestring

    def log_thread(self, context, pid):
        pass

    def log_call(self, context, apiname, category, arguments):
        apiindex, status, returnval, tid, timediff = context

        current_time = self.first_seen + datetime.timedelta(0, 0, timediff*1000)
        timestring = logtime(current_time)

        self.calls.append(self._parse([timestring,
                                  tid,
                                  category,
                                  apiname, 
                                  status,
                                  returnval] + arguments))
                                  
    def _parse(self, row):
        """Parse log row.
        @param row: row data.
        @return: parsed information dict.
        """
        call = {}
        arguments = []

        try:
            timestamp = row[0]    # Timestamp of current API call invocation.
            thread_id = row[1]    # Thread ID.
            category = row[2]     # Win32 function category.
            api_name = row[3]     # Name of the Windows API.
            status_value = row[4] # Success or Failure?
            return_value = row[5] # Value returned by the function.
        except IndexError as e:
            sys.stderr.write("Unable to parse process log row: %s" % e)
            return None

        # Now walk through the remaining columns, which will contain API
        # arguments.
        for index in range(6, len(row)):
            argument = {}

            # Split the argument name with its value based on the separator.
            try:
                arg_name, arg_value = row[index]
            except ValueError as e:
                sys.stderr.write("Unable to parse analysis row argument (row=%s): %s" % (row[index], e) )
                continue

            argument["name"] = arg_name

            argument["value"] = convert_to_printable(cleanup_value(arg_value))
            arguments.append(argument)

        call["timestamp"] = timestamp
        call["thread_id"] = str(thread_id)
        call["category"] = category
        call["api"] = api_name
        call["status"] = bool(int(status_value))

        if isinstance(return_value, int):
            call["return"] = "0x%.08x" % return_value
        else:
            call["return"] = convert_to_printable(cleanup_value(return_value))

        call["arguments"] = arguments
        call["repeated"] = 0

        return call


class BsonParser(object):
    def __init__(self, handler):
        self.handler = handler
        self.infomap = {}

        if not HAVE_BSON:
            sys.stderr.write("Starting BsonParser, but bson is not available! (install with `pip install bson`)")
            sys.exit(-1)

    def read_all(self):
        res = self.read_next_message()
        while res == True:
            res = self.read_next_message()
            
    def read_next_message(self):
        data = self.handler.read(4)
        if len(data) < 4:
            return False
        blen = struct.unpack("I", data)[0]
        if blen > MAX_MESSAGE_LENGTH:
            sys.stderr.write("BSON message larger than MAX_MESSAGE_LENGTH, stopping handler." + os.linesep*2)
            sys.exit(-1)

        data += self.handler.read(blen-4)

        try:
            dec = bson_decode(data)
        except Exception:
            sys.stderr.write("BsonParser decoding problem" + os.linesep*2)
            return False

        mtype = dec.get("type", "none")
        index = dec.get("I", -1)
        tid = dec.get("T", 0)
        time = dec.get("t", 0)

        #context = (apiindex, status, returnval, tid, timediff)
        context = [index, 1, 0, tid, time]

        if mtype == "info":
            # API call index info message, explaining the argument names, etc
            name = dec.get("name", "NONAME")
            arginfo = dec.get("args", [])
            category = dec.get("category")

            # Bson dumps that were generated before cuckoomon exported the
            # "category" field have to get the category using the old method.
            if not category:
                # Try to find the entry/entries with this api name.
                category = [_ for _ in LOGTBL if _[0] == name]

                # If we found an entry, take its category, otherwise we take
                # the default string "unknown."
                category = category[0][1] if category else "unknown"

            argnames, converters = check_names_for_typeinfo(arginfo)
            self.infomap[index] = name, arginfo, argnames, converters, category

        elif mtype == "debug":
            sys.stderr.write("Debug message from monitor: %s" % dec.get("msg", "") + os.linesep*2)

        elif mtype == "new_process":
            # new_process message from VMI monitor
            vmtime = datetime.datetime.fromtimestamp(dec.get("starttime", 0))
            procname = dec.get("name", "NONAME")
            ppid = 0
            modulepath = "DUMMY"

            self.handler.log_process(context, vmtime, None, ppid,
                                     modulepath, procname)

        else:
            # regular api call
            if not index in self.infomap:
                sys.stderr.write("Got API with unknown index - monitor needs to explain first" + os.linesep*2)
                return True

            apiname, arginfo, argnames, converters, category = self.infomap[index]
            args = dec.get("args", [])

            if len(args) != len(argnames):
                sys.stderr.write("Inconsistent arg count (compared to arg names)" + os.linesep*2)
                return True

            argdict = dict((argnames[i], converters[i](args[i]))
                           for i in range(len(args)))

            if apiname == "__process__":
                # special new process message from cuckoomon
                timelow = argdict["TimeLow"]
                timehigh = argdict["TimeHigh"]
                # FILETIME is 100-nanoseconds from 1601 :/
                vmtimeunix = (timelow + (timehigh << 32))
                vmtimeunix = vmtimeunix / 10000000.0 - 11644473600
                vmtime = datetime.datetime.fromtimestamp(vmtimeunix)

                pid = argdict["ProcessIdentifier"]
                ppid = argdict["ParentProcessIdentifier"]
                modulepath = argdict["ModulePath"]
                procname = get_filename_from_path(modulepath)

                self.handler.log_process(context, vmtime, pid, ppid,
                                         modulepath, procname)
                return True

            elif apiname == "__thread__":
                pid = argdict["ProcessIdentifier"]
                self.handler.log_thread(context, pid)
                return True

            context[1] = argdict.pop("is_success", 1)
            context[2] = argdict.pop("retval", 0)
            arguments = argdict.items()
            arguments += dec.get("aux", {}).items()

            self.handler.log_call(context, apiname, category, arguments)

        return True

#=============================================================================#

#lp = logProcessor("/Users/lorenzo/versioned/threattagger/cuckoo/storage/analyses/1/logs/1564.bson")
#lp.process()
#calls = lp.calls

def main():
    if len(sys.argv) != 2:
        sys.stderr.write("extract_syscalls.py <Cuckoo BSON log>" + os.linesep)
        sys.stderr.write("\t(CSV output will be written to a file in the current directory)" + os.linesep)
        sys.stderr.write("\t(Filename will be <PID>_<PARENTPID>_<PROCESS_NAME>.csv)" + os.linesep)
        sys.exit()
    
    lp = logProcessor(sys.argv[1])
    lp.process()
    lp.close()
    
    outF = open("%s_%s_%s.csv" % (str(lp.process_id), str(lp.parent_id), lp.process_name), 'w')
    outF.write("api,arguments,category,return,thread_id,timestamp\n")
    for call in lp.calls:
        outF.write('"' + call['api'] + '",')
        outF.write('"'+';'.join([':'.join([d['name'], d['value']]) for d in call['arguments']]).replace(',', '').replace('"', '').replace('\n', '\\n').replace('\r', '')+'",')
        outF.write('"' + call['category'] + '",')
        outF.write('"' + call['return'] + '",')
        outF.write('"' + call['thread_id'] + '",')
        outF.write('"' + call['timestamp'].replace(',', ':') + '"\n')
    outF.close()


if __name__ == "__main__":
    main()
