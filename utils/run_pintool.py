#!/usr/bin/env python2.7

import sys
import os
import subprocess
import shlex


if len(sys.argv) != 3:
    sys.stderr.write("Usage: run_pintool.py <pintool> <executable to analyze>" + os.linesep*2)
    sys.exit(-1)

# Assume this script is in the same folder as the submit.py script
myfolder = os.path.split(sys.argv[0])[0]
submit = os.path.join(myfolder, "submit.py")
custom_str = '"%s,%s"' % (sys.argv[1], "C:\\Program Files\\package\\" + os.path.basename(sys.argv[1]))
options_str = "pindll="+os.path.basename(sys.argv[1])

result = subprocess.call([submit, "--package", "pin", "--custom", custom_str, "--options", options_str, sys.argv[2]])
assert result == 0
